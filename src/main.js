import Vue from 'vue'
import './plugins/vuetify'
import Buefy from 'buefy'
import VRouter from 'vue-router'
import App from './App.vue'
import { routes } from './router.js'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)
Vue.use(VRouter);
Vue.config.productionTip = false


const router = new VRouter({
  routes,
  mode: 'history'
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
