
import Vue from 'vue'
import Router from 'vue-router'
import Asia from '@/components/Asia'
import Europa from '@/components/Europa'
import America from '@/components/America'
import Principal from '@/components/Principal'



Vue.use(Router)

export const routes = [
    {path: '', component: Principal},
    {path: '/Europa', component: Europa},
    {path: '/Asia', component: Asia},
    {path: '/America', component: America}
]